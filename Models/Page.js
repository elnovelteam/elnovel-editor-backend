'use strickt'

const Schema = mongoose.Schema;

const Page = new Schema({
    order: {
        type: Number,
        required: true
    },
    content: {
        type: Array,
        required: false
    },
    section_id: {
        type: Number,
        required: true,
    },
    created_at: {
        type: Date,
        required: false
    },
    updated_at: {
        type: Date,
        required: false
    }
},{
    timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }
})

module.exports = mongoose.model("Page", Page)