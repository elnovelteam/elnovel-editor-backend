'use strickt'
const Schema = mongoose.Schema;

const Section = new Schema({
    id: {
        type: Number,
        required: true,
    },
})

module.exports = mongoose.model('Section', Section)