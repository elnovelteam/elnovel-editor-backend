var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
// INCLUDING ROUTERS
var indexRouter    = require('./routes/index'),
	sectionsRouter = require('./routes/sections');

// STARTING THE APP
var app = express();

// DATABASE CONNECTION WITH MONGOOSE
let { DB_HOST, DB_PORT, DB_NAME } = require('./config/db.json');
global.mongoose = require("mongoose");
mongoose.connect(`mongodb://${DB_HOST}:${DB_PORT}/${DB_NAME}`, { useUnifiedTopology: true, useNewUrlParser: true });
mongoose.connection.once("open", function() {
  console.log(`MongoDB connection to database(${DB_NAME}) on mongodb://${DB_HOST}:${DB_PORT} established successfully`);
});
const Page = require('./Models/Page')
const Section = require('./Models/Section')

let blackpage = new Page({order: 1,content: [{telegram: true}], section_id: 1});
blackpage.save().then(() => console.log('inserted !')).catch(e => {if (e) console.log(e)})
console.log(blackpage);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/sections', sectionsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};
	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;
